#pragma once

#ifdef __cplusplus
extern "C" {
#endif

    int double_int(int);
    double double_float(double);
    char * double_str(char *);
    void str_delete(char *);

#ifdef __cplusplus
}
#endif
