#include <stdio.h>
#include "doubler.h"

int main(void)
{
    printf("%d\n", double_int(2));
    printf("%lf\n", double_float(1.3));

    char* str = double_str("Hi");
    printf("%s\n", str);
    str_delete(str);

    return 0;
}

