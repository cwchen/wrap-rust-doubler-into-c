var ref = require('ref');
var ffi = require('ffi');

var libdoubler = ffi.Library('./target/release/libdoubler', {
    'double_int': ['int', ['int']],
    'double_float': ['double', ['double']],
    'double_str': ['char *', ['char *']],
    'str_delete': ['void', ['char *']]
});

var doubler = {};

doubler.int = function (x) {
    "use strict";
    return libdoubler.double_int(x);
};

doubler.float = function (x) {
    "use strict";
    return libdoubler.double_float(x);
};

doubler.str = function (x) {
    "use strict";
    /* Allocate a C string in Node.js. */
    var buf = ref.allocCString(x);

    /* Run a C function in Ndoe.js. */
    var c_str = libdoubler.double_str(buf);

    /* Convert the C string into a Node.js string. */
    var s = ref.readCString(c_str);

    /* Clean system resource by C. */
    libdoubler.str_delete(c_str);

    return s;
};

module.exports = "doubler";

if (!module.parent) {
    console.log(doubler.int(2));
    console.log(doubler.float(1.3));
    console.log(doubler.str("Hi"));
}