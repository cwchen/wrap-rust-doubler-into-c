from cffi import FFI
import six

ffi = FFI()

ffi.cdef("""
int double_int(int);
double double_float(double);
char* double_str(char*);
void str_delete(char*);
""")

libdoubler = ffi.dlopen('target/release/libdoubler.so')

class Doubler:
    @classmethod
    def int(cls, x):
        return libdoubler.double_int(x)

    @classmethod
    def float(cls, x):
        return libdoubler.double_float(x)

    @classmethod
    def _str(cls, x):
        cstring = libdoubler.double_str(bytes(x, 'utf8'))
        yield ffi.string(cstring).decode('utf8')
        libdoubler.str_delete(cstring)

    @classmethod
    def str(cls, x):
        return six.next(cls._str(x))


if __name__ == '__main__':
    print(Doubler.int(2))
    print(Doubler.float(1.3))
    print(Doubler.str('Hi'))

