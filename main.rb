require 'ffi'

module MyLib
  extend FFI::Library

  ffi_lib 'c'
  ffi_lib 'target/release/libdoubler.so'

  attach_function :double_int, [:int], :int
  attach_function :double_float, [:double], :double
  attach_function :double_str, [:string], :string
  attach_function :str_delete, [:string], :void
end

class Doubler
  def self.int(x)
    MyLib::double_int(x)
  end

  def self.float(x)
    MyLib::double_float(x)
  end

  def self._str(x)
    Enumerator.new do |enum|
      # Run a C function in Ruby.
      string = MyLib::double_str(x)

      while true
        enum.yield string
      end

      # Clean system resource in C.
      MyLib::str_delete(string)
    end
  end

  def self.str(x)
    self._str(x).first
  end
end


if __FILE__ == $0 then
  puts Doubler.int(2)
  puts Doubler.float(1.3)
  puts Doubler.str('Hi')
end
