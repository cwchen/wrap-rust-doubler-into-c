use std::ffi::{CStr, CString};
use std::os::raw::c_char;

#[no_mangle]
pub extern "C" fn double_int(x: i32) -> i32 {
    x * 2
}

#[no_mangle]
pub extern "C" fn double_float(x: f64) -> f64 {
    x * 2.0
}

#[no_mangle]
pub extern "C" fn double_str(x: *const c_char) -> *const c_char {
    let string = unsafe { CStr::from_ptr(x).to_str().unwrap() };
    let output = format!("{}{}", string, string);
    CString::new(output).unwrap().into_raw()
}

#[no_mangle]
pub extern "C" fn str_delete(x: *mut c_char) {
    if x.is_null() {
        return
    }

    unsafe { Box::from_raw(x); }
}
